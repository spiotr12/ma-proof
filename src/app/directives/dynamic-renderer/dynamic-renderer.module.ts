import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicRendererDirective } from './dynamic-renderer.directive';


@NgModule({
  declarations: [DynamicRendererDirective],
  imports: [
    CommonModule,
  ],
  exports: [
    DynamicRendererDirective,
  ],
})
export class DynamicRendererModule {}
