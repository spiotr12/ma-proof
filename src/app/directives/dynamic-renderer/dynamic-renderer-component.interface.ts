export interface IDynamicRendererComponent<T = any> {
  data: T[];
}
