import { ComponentFactoryResolver, ComponentRef, Directive, Input, OnInit, Type, ViewContainerRef } from '@angular/core';
import { IDynamicRendererComponent } from './dynamic-renderer-component.interface';


/**
 * Creates dynamic component
 */
@Directive({
  selector: '[mpDynamicRenderer]',
})
export class DynamicRendererDirective implements OnInit {

  /**
   * Component class to create
   */
  @Input()
  public component: Type<IDynamicRendererComponent>;

  /**
   * Allows to pass additional params to created component
   */
  @Input()
  public componentParams: any;

  /**
   * Allows for custom operations
   */
  @Input()
  public componentSetupFn: (component: ComponentRef<IDynamicRendererComponent>) => void;

  @Input()
  public set data(value: any[]) {
    this._data = value;
    this.render();
  }

  public get data(): any[] { return this._data; }

  private _data: any[];

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef) {
  }

  ngOnInit(): void {
    this.render();
  }

  public render(): void {
    this.viewContainerRef.clear();

    const factory = this.componentFactoryResolver.resolveComponentFactory(this.component);
    const component = this.viewContainerRef.createComponent(factory);

    component.instance.data = this.data;

    if (this.componentParams) {
      Object.assign(component.instance, this.componentParams);
    }

    if (this.componentSetupFn) {
      this.componentSetupFn(component);
    }
  }
}
