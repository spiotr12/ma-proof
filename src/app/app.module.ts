import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTabsModule } from '@angular/material/tabs';
import { CardCollectionModule, ComponentSelectorModule, TableGridModule } from 'src/app/componentas';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatTabsModule,
    CardCollectionModule,
    ComponentSelectorModule,
    TableGridModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
