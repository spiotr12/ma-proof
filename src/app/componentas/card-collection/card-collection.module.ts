import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardCollectionComponent } from './card-collection.component';
import { MatCardModule } from '@angular/material/card';


@NgModule({
  declarations: [CardCollectionComponent],
  exports: [
    CardCollectionComponent,
  ],
  imports: [
    CommonModule,
    MatCardModule,
  ],
})
export class CardCollectionModule { }
