import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { IDynamicRendererComponent } from 'src/app/directives/dynamic-renderer';
import { ICardData } from './card-data.interface';




@Component({
  selector: 'mp-card-collection',
  templateUrl: './card-collection.component.html',
  styleUrls: ['./card-collection.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardCollectionComponent implements OnInit, IDynamicRendererComponent<ICardData> {

  @Input()
  public data: ICardData[];

  constructor() { }

  ngOnInit(): void {
  }

}
