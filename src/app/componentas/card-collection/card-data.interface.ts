export interface ICardData {
  title: string;
  text: string;
}
