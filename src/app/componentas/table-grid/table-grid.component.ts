import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IDynamicRendererComponent } from 'src/app/directives/dynamic-renderer';
import { ITableGridData } from './table-grid-data.interface';


@Component({
  selector: 'mp-table-grid',
  templateUrl: './table-grid.component.html',
  styleUrls: ['./table-grid.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableGridComponent implements OnInit, IDynamicRendererComponent<ITableGridData> {

  @Input()
  public data: ITableGridData[];

  @Output()
  public backup: EventEmitter<ITableGridData> = new EventEmitter<ITableGridData>();

  public columnsToDisplay = ['date', 'name', 'value', 'actions'];

  constructor() { }

  ngOnInit(): void {
  }

  backupClick(rowData: ITableGridData) {
    this.backup.emit(rowData);
  }
}
