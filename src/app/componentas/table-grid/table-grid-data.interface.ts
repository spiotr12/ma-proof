export interface ITableGridData {
  name: string;
  value: string | number;
  date: Date;
}
