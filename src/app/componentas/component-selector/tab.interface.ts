import { ComponentRef, Type } from '@angular/core';
import { Observable } from 'rxjs';
import { IDynamicRendererComponent } from 'src/app/directives/dynamic-renderer';


export interface ITab {
  /**
   * Tab title
   */
  title: string,

  /**
   * Data stream
   */
  data$: Observable<any[]>,

  /**
   * Component class to create
   */
  component: Type<any>,

  /**
   * Allows to pass additional params to created component
   */
  componentParams?: any;

  /**
   * Allows for custom operations
   */
  componentSetupFn?: (component: ComponentRef<IDynamicRendererComponent>) => void;
}
