import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ITab } from './tab.interface';


@Component({
  selector: 'mp-component-selector',
  templateUrl: './component-selector.component.html',
  styleUrls: ['./component-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentSelectorComponent {

  @Input()
  public tabs: ITab[];

  constructor() { }

}
