import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentSelectorComponent } from './component-selector.component';
import { MatTabsModule } from '@angular/material/tabs';
import { DynamicRendererModule } from 'src/app/directives/dynamic-renderer';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';



@NgModule({
  declarations: [ComponentSelectorComponent],
  exports: [
    ComponentSelectorComponent,
  ],
  imports: [
    CommonModule,
    MatTabsModule,
    DynamicRendererModule,
    MatToolbarModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class ComponentSelectorModule { }
