import { ChangeDetectionStrategy, Component, ComponentRef } from '@angular/core';
import { CardCollectionComponent, ICardData } from 'src/app/componentas/card-collection';
import { ITab, ITableGridData, TableGridComponent } from 'src/app/componentas';
import { BehaviorSubject, Subject } from 'rxjs';


/**
 * All data retrieving is within this component.
 * The child components are used only for displaying data, hence are independent or ony logic:
 * like pagination, sorting, filtering
 */
@Component({
  selector: 'mp-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {

  public cardCollectionData: Subject<ICardData[]> = new BehaviorSubject([
    { title: 'card 1', text: 'text 1' },
    { title: 'card 2', text: 'text 1' },
    { title: 'card 3', text: 'text 1' },
    { title: 'card 4', text: 'text 1' },
    { title: 'card 5', text: 'text 1' },
    { title: 'card 6', text: 'text 1' },
  ]);

  public tableGridData: Subject<ITableGridData[]> = new BehaviorSubject([
    { name: 'Name 1', date: new Date(), value: 'tytyryty' },
    { name: 'Name 2', date: new Date(), value: 'tytyryty' },
    { name: 'Name 3', date: new Date(), value: 'tytyryty' },
    { name: 'Name 4', date: new Date(), value: 'tytyryty' },
    { name: 'Name 5', date: new Date(), value: 'tytyryty' },
    { name: 'Name 6', date: new Date(), value: 'tytyryty' },
  ]);

  public componentSelectorTabs: ITab[] = [
    {
      title: 'Cards',
      component: CardCollectionComponent,
      data$: this.cardCollectionData,
    },
    {
      title: 'Table',
      component: TableGridComponent,
      data$: this.tableGridData,
      componentSetupFn: (component: ComponentRef<TableGridComponent>) => {
        component.instance.backup.subscribe((data) => {
          console.log('Data retrieved from dynamically created component', data);
        });
      },
    },
  ];

  public randomizeCardData() {
    const length = Math.ceil(Math.random() * 20) + 5;
    this.cardCollectionData.next(Array.from({ length }, (_, index) => ({
      title: `card ${index}`,
      text: `text ${index}`,
    })));
  }

  public randomizeTableData() {
    const length = Math.ceil(Math.random() * 20) + 5;
    this.tableGridData.next(Array.from({ length }, (_, index) => ({
      name: `name ${index}`,
      date: new Date(),
      value: `text ${index}`,
    })));
  }
}
